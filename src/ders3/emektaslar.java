package ders3;

public class emektaslar {
    private String ademektas;
    private String soyademekas;
    private int yasemektas;
    private  String kariemektas;
    private int stajemektas;

    public emektaslar() {
    }

    public emektaslar(String ademektas, String soyademekas, int yasemektas, String kariemektas, int stajemektas) {
        this.ademektas = ademektas;
        this.soyademekas = soyademekas;
        this.yasemektas = yasemektas;
        this.kariemektas = kariemektas;
        this.stajemektas = stajemektas;
    }

    @Override
    public String toString() {
        return "emektaslar{" +
                "ademektas='" + ademektas + '\'' +
                ", soyademekas='" + soyademekas + '\'' +
                ", yasemektas=" + yasemektas +
                ", kariemektas='" + kariemektas + '\'' +
                ", stajemektas=" + stajemektas +
                '}';
    }

    public String getAdemektas() {
        return ademektas;
    }

    public void setAdemektas(String ademektas) {
        this.ademektas = ademektas;
    }

    public String getSoyademekas() {
        return soyademekas;
    }

    public void setSoyademekas(String soyademekas) {
        this.soyademekas = soyademekas;
    }

    public int getYasemektas() {
        return yasemektas;
    }

    public void setYasemektas(int yasemektas) {
        this.yasemektas = yasemektas;
    }

    public String getKariemektas() {
        return kariemektas;
    }

    public void setKariemektas(String kariemektas) {
        this.kariemektas = kariemektas;
    }

    public int getStajemektas() {
        return stajemektas;
    }

    public void setStajemektas(int stajemektas) {
        this.stajemektas = stajemektas;
    }
}
