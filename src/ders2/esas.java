package ders2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class esas {
    JFrame esas;
    String data;

    esas() {

        esas = new JFrame("Basliq");
        esas.setBounds(300, 250, 400,380);
        esas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel yazi=new JLabel();
        yazi.setLocation(0, 10);

        // create an empty combo box with items of type String
        String idareler[]={"nazrlik","alikateqoruya","public"};
        JComboBox idarenovu=new JComboBox(idareler);
        idarenovu.addActionListener (new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String secim= (String) idarenovu.getSelectedItem();

                //System.out.print(secim+" ");

                ApplicationContext qaynaq=new ClassPathXmlApplicationContext("ders2/veri.xml");

                idare cavab= (idare) qaynaq.getBean(secim);
                data=cavab.toString();
                ((ClassPathXmlApplicationContext) qaynaq).close();

                //System.out.print(data);
                yazi.setText(data);
            }
        });

        JPanel panel=new JPanel();
        panel.setBounds(8, 30,200, 50);
        idarenovu.setLocation(0, 0);
        panel.add(idarenovu);

        esas.add(panel);
        esas.add(yazi);
        esas.setVisible(true);
    }

    public static void main(String[] args) {
        new esas();
    }
}