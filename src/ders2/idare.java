package ders2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class idare {
    private String ad;
    private String  tarix;
    //SimpleDateFormat tarixformat=new SimpleDateFormat("DD.MM.YY");
    //Date zaman = tarixformat.parse(tarix);

    public idare(){
    }

    public idare(String ad, String tarix) {
        this.ad = ad;
        this.tarix = tarix;
    }

    @Override
    public String toString() {
        return "idare{" +
                "ad='" + ad + '\'' +
                ", tarix='" + tarix + '\'' +
                '}';
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getTarix() {
        return tarix;
    }

    public void setTarix(String tarix) {
        this.tarix = tarix;
    }
}