package ders1;

public class idare {
    private  String adi;
    private int iscisy;

    public idare(String adi, int iscisy) {
        this.adi = adi;
        this.iscisy = iscisy;
    }

    @Override
    public String toString() {
        return "ders1.idare{" +
                "adi='" + adi + '\'' +
                ", iscisy=" + iscisy +
                '}';
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public int getIscisy() {
        return iscisy;
    }

    public void setIscisy(int iscisy) {
        this.iscisy = iscisy;
    }
}
