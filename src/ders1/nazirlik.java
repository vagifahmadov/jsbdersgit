package ders1;

public class nazirlik {
    private String nazadi;
    private int iscisay;
    private idare tabet;

    public nazirlik(String nazadi, int iscisay, idare tabet) {
        this.nazadi = nazadi;
        this.iscisay = iscisay;
        this.tabet = tabet;
    }

    @Override
    public String toString() {
        return "ders1.nazirlik{" +
                "nazadi='" + nazadi + '\'' +
                ", iscisay=" + iscisay +
                ", tabet=" + tabet +
                '}';
    }

    public String getNazadi() {
        return nazadi;
    }

    public void setNazadi(String nazadi) {
        this.nazadi = nazadi;
    }

    public int getIscisay() {
        return iscisay;
    }

    public void setIscisay(int iscisay) {
        this.iscisay = iscisay;
    }

    public idare getTabet() {
        return tabet;
    }

    public void setTabet(idare tabet) {
        this.tabet = tabet;
    }
}
